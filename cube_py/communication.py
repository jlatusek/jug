from typing import Tuple

import serial


class DataValidationHasNotBeenCalled(Exception):
    pass


class DataIsInvalid(Exception):
    pass


class Communication:
    def __init__(self, handler_name: str):
        self._handler_name = handler_name
        self._data_is_valid = True
        self._data_has_been_validated = False
        self._serial = None
        self._raw_data = None
        self._data = None
        self._acc = None
        self._mag = None

    def connect(self):
        self._serial = serial.Serial(
            self._handler_name, timeout=1, parity=serial.PARITY_ODD
        )

    def read_data(self):
        self._raw_data = self._serial.readline()
        self._data_has_been_validated = False

    def _parse_data(self):
        try:
            decoded_data = self._raw_data.decode("utf-8")
        except UnicodeDecodeError as e:
            self._data_is_valid = False
            return
        self._data = decoded_data.split(" ")
        if self._data[0] == "73":
            self._data_is_valid = True
        else:
            self._data_is_valid = False
        try:
            self._acc = [int(x) for x in self._data[1:4]]
            self._mag = [int(x) for x in self._data[4:7]]
        except ValueError as e:
            self._data_is_valid = False
        self._data_has_been_validated = True

    @property
    def data_is_valid(self) -> bool:
        self._parse_data()
        return self._data_is_valid

    @property
    def data(self) -> ((int, int, int), (int, int, int)):
        if not self._data_has_been_validated:
            raise DataValidationHasNotBeenCalled()
        if self._data_is_valid:
            return self._acc, self._mag
        else:
            raise DataIsInvalid()

    @property
    def raw_acc(self) -> (int, int, int):
        if not self._data_has_been_validated:
            raise DataValidationHasNotBeenCalled()
        if self._data_is_valid:
            return self._acc
        else:
            raise DataIsInvalid()

    @property
    def raw_mag(self) -> (int, int, int):
        if not self._data_has_been_validated:
            raise DataValidationHasNotBeenCalled()
        if self._data_is_valid:
            return self._mag
        else:
            raise DataIsInvalid()
