import math

import numpy as np
from OpenGL.GL import *
from OpenGL.GLU import *
from scipy.spatial.transform import Rotation as R


class Cube:

    # location of each vertex in space, points connected would form a cube
    vertices = (
        (1, -1, -1),
        (1, 1, -1),
        (-1, 1, -1),
        (-1, -1, -1),
        (1, -1, 1),
        (1, 1, 1),
        (-1, -1, 1),
        (-1, 1, 1),
    )
    # each tuple contains of two vertices
    edges = (
        (0, 1),
        (0, 3),
        (0, 4),
        (2, 1),
        (2, 3),
        (2, 7),
        (6, 3),
        (6, 4),
        (6, 7),
        (5, 1),
        (5, 4),
        (5, 7),
    )

    surfaces = (
        (0, 1, 2, 3),
        (3, 2, 7, 6),
        (6, 7, 5, 4),
        (4, 5, 1, 0),
        (1, 5, 7, 2),
        (4, 0, 3, 6),
    )

    colors = (
        (1, 0, 0),
        (1, 0, 1),
        (0, 0, 1),
        (0, 0, 0),
        (1, 1, 1),
        (1, 0, 1),
        (1, 0, 0),
        (1, 0, 1),
        (0, 0, 1),
        (0, 0, 0),
        (1, 1, 1),
        (1, 0, 1),
    )

    def __init__(self):
        self._rotation = None
        self._vertices_rotation = []

    def calculate_points_coordination_from_rotation(self, rotation):
        self._vertices_rotation = rotation.apply(Cube.vertices).tolist()

    def calculate_points_coordination(self, roll, pitch, yaw):
        """
        Change order of passed angle, select experimentally to looks good
        """
        self._rotation = R.from_euler("xyz", [roll, pitch, yaw], degrees=True)
        self._vertices_rotation = self._rotation.apply(Cube.vertices).tolist()

    def draw(self):
        glBegin(GL_QUADS)
        for surface in Cube.surfaces:
            x = 0
            for vertex in surface:
                x += 1
                glColor3fv(Cube.colors[x])
                # glVertex3fv(Cube.vertices[vertex])
                glVertex3fv(self._vertices_rotation[vertex])
        glEnd()

        glBegin(GL_LINES)
        # notifies OPENGL that we're about to code smth with aim to draw lines
        # (hence GLLINES)
        for edge in Cube.edges:
            for vertex in edge:
                glVertex3fv(self._vertices_rotation[vertex])
        glEnd()
