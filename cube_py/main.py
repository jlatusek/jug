import numpy as np
import pygame
from OpenGL.raw.GL.VERSION.GL_1_0 import (
    glTranslatef,
    glRotatef,
    glClear,
    GL_COLOR_BUFFER_BIT,
    GL_DEPTH_BUFFER_BIT,
)
from OpenGL.raw.GLU import gluPerspective
from pygame.constants import DOUBLEBUF, OPENGL

from angledetection import (
    Filter,
    AngleDetection,
    ECompassAngleDetection,
    AndroidAngleDetection,
)
from cube import Cube
from communication import Communication

from OpenGL.GL import *
from OpenGL.GLU import *


class SpiCube:
    def __init__(self):
        self.communication = Communication("/dev/ttyUSB0")
        self._display = (
            800,
            600,
        )
        self.acc_filter = Filter()
        self.mag_filter = Filter()
        # self.angle_detection = AngleDetection()
        # self.angle_detection = ECompassAngleDetection()
        self.angle_detection = AndroidAngleDetection()

        self.cube = Cube()

    def filters_init(self,):
        while not all((self.acc_filter.is_data_matrix_filled,)):
            self.communication.read_data()
            if self.communication.data_is_valid:
                acceleration, magnetic = self.communication.data
                self.acc_filter.add_raw_data(acceleration)
                self.mag_filter.add_raw_data(magnetic)
        while not all(
            (
                self.acc_filter.is_median_matrix_filled,
                self.mag_filter.is_median_matrix_filled,
            )
        ):
            acceleration, magnetic = self.communication.data
            self.acc_filter.add_raw_data(acceleration)
            self.mag_filter.add_raw_data(magnetic)
            self.acc_filter.get_data()
            self.mag_filter.get_data()

    def display_init(self):
        pygame.init()

        pygame.display.set_mode(self._display, DOUBLEBUF | OPENGL)
        # telling we're gonna use opengl and doublebuffer
        # (to comply with monitro refresh rate
        gluPerspective(45, (self._display[0] / self._display[1]), 0.1, 50.0)
        # 45-degree, second-acpect ration width/height,
        # last to znear zfar clipping planes (tak jakby perspektywa)
        # glTranslatef(0.0, 0.0, -5)
        glTranslatef(0.0, 0.0, -5)
        glRotatef(270, 1, 0, 0)
        glRotatef(90, 0, 0, 1)
        # we're moving back 5 unites so we can see th cube

    def init(self):
        self.communication.connect()
        self.filters_init()
        self.angle_detection.compute_rotation_angles(
            self.acc_filter.get_data(), self.mag_filter.get_data(),
        )
        self.display_init()
        self.init_rotation = self.angle_detection.rotation.inv()

    def draw_axis(self):
        axis = [
            (0, 0, 0),
            (100, 0, 0),
            (0, 0, 0),
            (0, 100, 0),
            (0, 0, 0),
            (0, 0, 100),
        ]
        colors = (
            (1, 0, 0),
            (0, 1, 0),
            (0, 0, 1),
        )

        glBegin(GL_LINES)
        for i, vert in enumerate(axis):
            glColor3fv(colors[int(i / 2)])
            glVertex3fv(vert)
        glEnd()

    def display_loop(self):
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()

            self.communication.read_data()
            if self.communication.data_is_valid:
                self.acc_filter.add_raw_data(self.communication.raw_acc)
                self.mag_filter.add_raw_data(self.communication.raw_mag)
                self.angle_detection.compute_rotation_angles(
                    self.acc_filter.get_data(), self.mag_filter.get_data(),
                )

            # print(self.angle_detection.rotation_angles_deg)

            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
            # roll, pitch, yaw = self.angle_detection.rotation_angles_deg
            # self.cube.calculate_points_coordination(roll, -pitch, yaw)
            r = self.angle_detection.rotation
            # euler = r.as_euler("xyz")
            # euler[0] = -euler[0]
            # r = r.from_euler("xyz", euler)

            self.cube.calculate_points_coordination_from_rotation(
                r * self.init_rotation
            )

            self.cube.draw()
            self.draw_axis()
            pygame.display.flip()  # updates display
            pygame.time.wait(1)


if __name__ == "__main__":
    spi_cube = SpiCube()
    spi_cube.init()
    spi_cube.display_loop()
