import numpy as np
from scipy.spatial.transform import Rotation as R
import math


class Filter:
    length = 5

    def __init__(self):
        self._median_matrix = np.array([[0, 0, 0]])
        self._raw_data_matrix = []
        self._average_matrix = []

    def _sliding_median_filter(self):
        norm_matrix = []

        for item in self._raw_data_matrix:
            norm = np.linalg.norm(
                item, ord=1
            )  # nuclear norm or frobenius norm???
            norm_matrix.append(norm)

        median_value = np.median(norm_matrix)
        where_out = np.where(norm_matrix == median_value)
        index = where_out[0][0]
        filtered_value = self._raw_data_matrix[index]
        self._median_matrix = np.append(
            self._median_matrix, [filtered_value], axis=0
        )
        while len(self._median_matrix) > Filter.length:
            self._median_matrix = self._median_matrix[1:]

    def _sliding_average_filter(self):
        sum_matrix = sum(self._median_matrix)
        self._average_matrix = sum_matrix / len(self._median_matrix)

    def get_data(self) -> (int, int, int):
        """

        Returns:

        """
        self._sliding_median_filter()
        self._sliding_average_filter()
        return self._average_matrix

    def add_raw_data(self, raw_data: (int, int, int)):
        self._raw_data_matrix.append(raw_data)
        while len(self._raw_data_matrix) > Filter.length:
            self._raw_data_matrix.pop(0)

    @property
    def is_data_matrix_filled(self) -> bool:
        if len(self._raw_data_matrix) == Filter.length:
            return True
        return False

    @property
    def is_median_matrix_filled(self) -> bool:
        if len(self._median_matrix) == Filter.length:
            return True
        return False


class AngleDetection:
    def __init__(self):
        self._roll = 0
        self._yaw = 0
        self._pitch = 0
        self._prev_roll = 0
        self._prev_yaw = 0
        self._prev_pitch = 0
        self._singularity_detected = False
        self._acceleration = None
        self._magnetic = None
        self._delta = 1.15

    def detect_singularity(self):
        norm_acc_value = np.linalg.norm(self._acceleration, ord=1)
        normalized_grav_field = self._acceleration / norm_acc_value

        norm_mag_value = np.linalg.norm(self._magnetic)
        normalized_mag_field = self._magnetic / norm_mag_value

        if any(
            np.allclose(
                [
                    normalized_grav_field[0],
                    normalized_grav_field[1],
                    normalized_mag_field[0],
                    normalized_mag_field[1],
                ],
                vector,
                atol=0.1,
                rtol=0,
            )
            for vector in (
                [1, 0, np.sin(self._delta), np.cos(self._delta)],
                [1, 0, np.sin(self._delta), -np.cos(self._delta)],
                [0, -1, np.cos(self._delta), -np.sin(self._delta)],
                [0, -1, -np.cos(self._delta), -np.sin(self._delta)],
                [-1, 0, -np.sin(self._delta), -np.cos(self._delta)],
                [-1, 0, -np.sin(self._delta), np.cos(self._delta)],
                [0, -1, -np.cos(self._delta), np.sin(self._delta)],
                [0, -1, np.cos(self._delta), np.sin(self._delta)],
            )
        ):
            self._singularity_detected = True
        else:
            self._singularity_detected = False

    def _compute_roll_angle(self):
        self._roll = np.arctan2(self._acceleration[1], self._acceleration[2])

    def _compute_pitch_angle(self):
        nominator = -self._acceleration[0]
        denominator = self._acceleration[1] * np.sin(
            self._roll
        ) + self._acceleration[2] * np.cos(self._roll)
        angle = nominator / denominator
        self._pitch = np.arctan(angle)

    def _compute_yaw_angle(self):
        nominator = self._magnetic[2] * np.sin(self._roll) - self._magnetic[
            1
        ] * np.cos(self._roll)
        denominator = (
            self._magnetic[0] * np.cos(self._pitch)
            + self._magnetic[1] * np.sin(self._roll) * np.sin(self._pitch)
            + self._magnetic[2] * np.sin(self._pitch) * np.cos(self._roll)
        )
        self._yaw = np.arctan2(nominator, denominator)

    def _store_prev_angles_val(self):
        self._prev_roll = self._roll
        self._prev_pitch = self._pitch
        self._prev_yaw = self._yaw

    def compute_rotation_angles(
        self, acceleration: np.array, magnetic: np.array
    ):
        """
        Compute rotation angles: roll, pitch, yaw. Detects the singularity
        that if occurs.
        """
        self._acceleration = acceleration
        self._magnetic = magnetic
        self._store_prev_angles_val()
        self.detect_singularity()
        if self._singularity_detected:
            return
        else:
            self._compute_roll_angle()
            self._compute_pitch_angle()
            self._compute_yaw_angle()

        self._r = R.from_euler("xyz", [self._roll, self._pitch, self._yaw])

    @property
    def rotation_angles(self) -> (float, float, float):
        return self._roll, self._pitch, self._yaw

    @property
    def rotation_angles_deg(self) -> (float, float, float):
        return (
            self._roll * 180 / np.pi,
            self._pitch * 180 / np.pi,
            self._yaw * 180 / np.pi,
        )

    @property
    def previous_rotation_angles(self) -> (float, float, float):
        return self._prev_roll, self._prev_pitch, self._prev_yaw

    @property
    def previous_rotation_angles_deg(self) -> (float, float, float):
        return (
            self._prev_roll * 180 / np.pi,
            self._prev_pitch * 180 / np.pi,
            self._prev_yaw * 180 / np.pi,
        )

    @property
    def rotation_diff(self) -> (float, float, float):
        return (
            self._roll - self._prev_roll,
            self._prev_pitch - self._prev_pitch,
            self._yaw - self._prev_yaw,
        )

    @property
    def rotation_diff_deg(self) -> (float, float, float):
        return (
            (self._roll - self._prev_roll) * 180 / np.pi,
            (self._pitch - self._prev_pitch) * 180 / np.pi,
            (self._yaw - self._prev_yaw) * 180 / np.pi,
        )

    @property
    def rotation(self):
        return self._r


class ECompassAngleDetection(AngleDetection):
    def __init__(self):
        super().__init__()
        self.V = [
            474.62387146,
            423.66495911,
            215.85177874,
        ]

    def compute_rotation_angles(
        self, acceleration: np.array, magnetic: np.array
    ):
        self._acceleration = acceleration
        self._magnetic = magnetic
        self._store_prev_angles_val()
        self._compute_roll_angle()
        self._compute_pitch_angle()
        self._compute_yaw_angle()
        self._r = R.from_euler("xyz", [self._roll, self._pitch, self._yaw])

    def _compute_roll_angle(self):
        self._roll = np.arctan2(self._acceleration[1], self._acceleration[2])

    def _compute_pitch_angle(self):
        nominator = -self._acceleration[0]
        denominator = self._acceleration[1] * np.sin(
            self._roll
        ) + self._acceleration[2] * np.cos(self._roll)
        self._pitch = np.arctan(nominator / denominator)

    def _compute_yaw_angle(self):
        bpx = self.bpx
        bpy = self.bpy
        bpz = self.bpz
        Vx = self.Vx
        Vy = self.Vy
        Vz = self.Vz
        roll = self._roll
        pitch = self._pitch

        m_b_fy = ((bpz - Vz) * np.sin(roll)) - ((bpy - Vy) * np.cos(roll))
        b_fx = (
            (bpx - Vx) * np.cos(self._pitch)
            + (bpy - Vy) * np.sin(pitch) * np.sin(roll)
            + (bpz - Vz) * np.sin(pitch) * np.cos(roll)
        )
        self._yaw = np.arctan2(m_b_fy, b_fx)
        self._r = R.from_euler("xyz", [self._roll, self._pitch, self._yaw])


    @property
    def bpx(self):
        return self._magnetic[0]

    @property
    def bpy(self):
        return self._magnetic[1]

    @property
    def bpz(self):
        return self._magnetic[2]

    @property
    def Vx(self):
        return self.V[0]

    @property
    def Vy(self):
        return self.V[1]

    @property
    def Vz(self):
        return self.V[2]


class AndroidAngleDetection(AngleDetection):
    def compute_rotation_angles(
        self, acceleration: np.array, magnetic: np.array
    ):
        gravity = acceleration
        geomagnetic = magnetic

        Ax = gravity[0]
        Ay = gravity[1]
        Az = gravity[2]
        Ex = geomagnetic[0]
        Ey = geomagnetic[1]
        Ez = geomagnetic[2]
        Hx = Ey * Az - Ez * Ay
        Hy = Ez * Ax - Ex * Az
        Hz = Ex * Ay - Ey * Ax
        normH = math.sqrt(Hx * Hx + Hy * Hy + Hz * Hz)
        if normH < 0.1:
            # device is close to free fall (or in space?), or close to
            # magnetic north pole. Typical values are  > 100.
            return None
        invH = 1.0 / normH
        Hx *= invH
        Hy *= invH
        Hz *= invH
        invA = 1.0 / math.sqrt(Ax * Ax + Ay * Ay + Az * Az)
        Ax *= invA
        Ay *= invA
        Az *= invA
        Mx = Ay * Hz - Az * Hy
        My = Az * Hx - Ax * Hz
        Mz = Ax * Hy - Ay * Hx

        self._r = R.from_matrix([[Hx, Hy, Hz,], [Mx, My, Mz,], [Ax, Ay, Az,]])
        self._roll, self._pitch, self._yaw = self._r.as_euler(
            "xyz", degrees=True
        )

        # if (I != null) {
        #     // compute the inclination matrix by projecting the geomagnetic
        #     // vector onto the Z (gravity) and X (horizontal component
        #     // of geomagnetic vector) axes.
        #     final float invE = 1.0f / (float)Math.sqrt(Ex*Ex + Ey*Ey + Ez*Ez);
        #     final float c = (Ex*Mx + Ey*My + Ez*Mz) * invE;
        #     final float s = (Ex*Ax + Ey*Ay + Ez*Az) * invE;
        #     if (I.length == 9) {
        #         I[0] = 1;     I[1] = 0;     I[2] = 0;
        #         I[3] = 0;     I[4] = c;     I[5] = s;
        #         I[6] = 0;     I[7] =-s;     I[8] = c;
        #     } else if (I.length == 16) {
        #         I[0] = 1;     I[1] = 0;     I[2] = 0;
        #         I[4] = 0;     I[5] = c;     I[6] = s;
        #         I[8] = 0;     I[9] =-s;     I[10]= c;
        #         I[3] = I[7] = I[11] = I[12] = I[13] = I[14] = 0;
        #         I[15] = 1;
        #     }
        # }


