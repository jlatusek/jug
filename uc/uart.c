#define BAUD 9600

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/setbaud.h>
#include <stdarg.h>
#include <string.h>
#include <stdio.h>

#include "uart.h"
#include "led.h"


volatile sendBuff uart_tx_buff = {
    .start=0, .end=0, .empty=1
};

void uart_init()
{
    sei();
    UBRR0 = F_CPU/((uint32_t)16*BAUD);
    UCSR0B  = _BV(RXEN0) | _BV(TXEN0) | _BV(RXCIE0) | _BV(TXCIE0); 
    UCSR0C = _BV(USBS0) | (3<< UCSZ00);
}

ISR(USART_TX_vect)
{
    uart_print_buff();
}

ISR(USART_RX_vect)
{
    uint8_t val = UDR0;
    val ++;
    led_toggle();
}

void uart_putchar(unsigned char data)
{
    while(!(UCSR0A & _BV(UDRE0)));
    UDR0 = data;
}

uint8_t uart_printf(const char *format, ...)
{
    va_list args;
    va_start(args, format);
    int size = vsnprintf( (char *)&uart_tx_buff.buff[uart_tx_buff.end],
                sizeof(uart_tx_buff.buff), format, args);
    va_end(args);
    uart_tx_buff.empty = 0;
    uart_tx_buff.end += size;
    uart_print_buff();
    return 0;

}

void uart_print_buff()
{
    if(uart_tx_buff.start < uart_tx_buff.end)
    {
        uart_putchar(uart_tx_buff.buff[uart_tx_buff.start]);
        uart_tx_buff.start++;
    }
    else
    {
        uart_tx_buff.empty = 1;
        uart_tx_buff.start = 0;
        uart_tx_buff.end = 0;
    }
}
