#include <stdint.h>

void spi_init();
void spi_transciever(uint8_t *data_in, uint8_t *data_out, uint8_t len);
uint8_t spi_master_transmit_receive(uint8_t data);
void spi_transmit(uint8_t *data_out, uint8_t len);
void spi_cs();
void spi_cus();
