#include <stdint.h>

typedef struct
{
    char buff[128];
    uint8_t start;
    uint8_t end;
    uint8_t empty;

}sendBuff;


void uart_init();
void uart_putchar(unsigned char data);
void uart_print_buff();
uint8_t uart_printf(const char *format, ...);
