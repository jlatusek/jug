#include <avr/io.h>
#include <util/delay.h>

#include "led.h"
#include "btn.h"
#include "uart.h"
#include "spi.h"
#include "accelerometer.h"

#define F_CPU 16000000L


void printBits(uint8_t size, void * ptr)
{
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int i, j;

    for (i=size-1;i>=0;i--)
    {
        for (j=7;j>=0;j--)

        {
            byte = (b[i] >> j) & 1;
            uart_printf("%u", byte);
        }
    }
}

void int_test()
{
    while(1)
    {
        uint8_t a = 0b01111111;
        uint8_t b = 0xFF;
        int16_t c = (a<<8) | b;
        uart_printf("a=%u\r\n",a);
        printBits(1,&a);
        uart_printf("\r\n");
        uart_printf("b=%d\r\n",b);
        printBits(1,&b);
        uart_printf("\r\n");
        uart_printf("c=%d\r\n",c);
        printBits(2,&c);
        uart_printf("\r\n");
        _delay_ms(1000);
    }
}


int main()
{
    uart_init();
    led_init();
    spi_init();

    lsm303d_init();
    accelerometer_value acc, mag;
    while(1)
    {
        uint8_t id = get_id();
        acc = concatenate_acceleration();
        mag = concatenate_magnetic_field();
        uart_printf("%d %d %d %d %d %d %d\r\n",id, 
                acc.x, acc.y, acc.z, mag.x, mag.y, mag.z);
        _delay_ms(100);
    }
    return 0;
}
