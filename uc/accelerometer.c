#include "accelerometer.h"
#include "spi.h"

static uint8_t data_out_buff[255];
static uint8_t data_in_buff[255];

void lsm303d_init()
{
    //enable accelerometer
    write_reg(CTRL1, 0x37); 
    write_reg(CTRL7, 0x00);
    //enable magnetometer and temperature
    write_reg(CTRL5, 0x90);
}

void magnetometer_init()
{
}

void write_reg(uint8_t addres, uint8_t value)
{
    uint8_t data_out[2] = {addres, value};
    spi_transmit(data_out, 2);
}

void read_reg(uint8_t addres, uint8_t *data_in, uint8_t len)
{
    data_out_buff[0] = addres;
    spi_transciever(data_in, data_out_buff, len+1);
}

accelerometer_value concatenate_acceleration()
{
    return mulitead_register(OUT_X_L_A);
}

accelerometer_value concatenate_magnetic_field()
{
    return mulitead_register(OUT_X_L_M);
}

accelerometer_value mulitead_register(uint8_t register_addres)
{
    read_reg(register_addres | READDATA | MULTIREAD, data_in_buff, 6);
    accelerometer_value val = {
        .x=data_in_buff[2] << 8 | data_in_buff[1],
        .y=data_in_buff[4] << 8 | data_in_buff[3],
        .z=data_in_buff[6] << 8 | data_in_buff[5],
    };
    return val;
}

uint8_t get_id()
{
    read_reg(WHO_AM_I | READDATA, data_in_buff, 1);
    return data_in_buff[1];
}
