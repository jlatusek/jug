#include <stdint.h>

#include "LSM303D.h"
#include "spi.h"

typedef struct{
    int16_t x;
    int16_t y;
    int16_t z;
}accelerometer_value;

void lsm303d_init();
void write_reg(uint8_t addres, uint8_t value);
void read_reg(uint8_t addres, uint8_t *data_in, uint8_t len);
accelerometer_value concatenate_acceleration();
accelerometer_value mulitead_register(uint8_t register_addres);
accelerometer_value concatenate_magnetic_field();
uint8_t get_id();
