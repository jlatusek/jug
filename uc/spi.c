#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "spi.h"

#define SS_PIN PB2
#define MOSI_PIN PB3
#define MISO_PIN PB4
#define SCK_PIN PB5 


void wait(uint16_t tics)
{
    uint16_t i;
    for(i=0;i<tics;++i)
    {
        asm("nop");
    }
}


void spi_init()
{
    DDRB = _BV(MOSI_PIN) | _BV(SCK_PIN) | _BV(SS_PIN);
    SPCR = _BV(SPE) | _BV(MSTR) | _BV(CPOL) | _BV(SPR1) | _BV(SPR0) |_BV(CPHA);
    spi_cus();
}


void spi_transciever(uint8_t *data_in, uint8_t *data_out, uint8_t len)
{

    uint8_t i;
    spi_cs();
    for(i=0 ;i<len;++i)
    {
        data_in[i] = spi_master_transmit_receive(data_out[i]);
    }
    spi_cus();
}

void spi_transmit(uint8_t *data_out, uint8_t len)
{

    uint8_t i;
    spi_cs();
    for(i=0 ;i<len;++i)
    {
        spi_master_transmit_receive(data_out[i]);
    }
    spi_cus();
}
inline uint8_t spi_master_transmit_receive(uint8_t data)
{
    SPDR = data;
    while(!(SPSR & _BV(SPIF)));
    return SPDR;
}

void spi_cs()
{
    PORTB &= ~(_BV(SS_PIN)); 
}

void spi_cus()
{
    PORTB |= _BV(SS_PIN); 
}
