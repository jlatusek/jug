#include <avr/io.h>

#define BTN_MASK PB1

void init_btn()
{
	//set as input
	DDRB &= ~BTN_MASK;
	PORTB |= BTN_MASK;
}

uint8_t btn_state()
{
	return !(PINB & ~BTN_MASK);
}
