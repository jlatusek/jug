#include <avr/io.h>

#include "led.h"

#define LED_MASK 0x20

void led_init()
{
    // led pin B5
    // DDRx define input or output, if 1 is output
    DDRB |= _BV(PB5);
}

void led_on()
{
	// set value on specific pin
    PORTB |= LED_MASK; 
}

void led_off()
{
    PORTB &= ~LED_MASK;
}

void led_toggle()
{
    PORTB ^= LED_MASK;
}
